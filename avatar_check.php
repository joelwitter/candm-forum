<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>phpBB3 Avatar Checking Tool</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
  <!--
body 
  {
  font-family: Arial, heltetica, verdana, sans-serif;
  }
h1 {
font-size: 1.2em;
}
-->
</style>
</head>
<body>
<h1>phpBB3 Avatar Checking Tool</h1>

	<form method="post" action="<?php $_SERVER["PHP_SELF"]; ?>">

  	User name to check:
  <input type="text" name="username" />
	<br />~or~
	<br />User id to check:
  <input type="text" name="user_id" />
  <input type="hidden" name="act" value="submit" />
  <input type="submit" name="submit" value="Check" />
	</form>

<?php
define('IN_PHPBB', true);
$phpbb_root_path = ((isset($phpbb_root_path)) ? $phpbb_root_path : './');
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
require($phpbb_root_path . '/includes/functions_display.' . $phpEx);

// Start session management
$user->session_begin();
$auth->acl($user->data);
$user->setup('posting');

$submit = (isset($_POST['submit'])) ? true : false;

$user_id = request_var('user_id',0);
$username = request_var('username','');

echo '<br />';

if ($submit)
{
	$sql = 'SELECT user_id, username, user_avatar, user_avatar_type, user_avatar_width, user_avatar_height FROM ' . USERS_TABLE;

	if (!empty($_POST['username']))
	{
		$sql .= " WHERE username_clean = '" . $db->sql_escape(utf8_clean_string($username)) . "'";
	}
	if (!empty($_POST['user_id']))
	{
		$sql .= ' WHERE user_id = ' . (int) $user_id;
	}

	$result = $db->sql_query($sql);
	$row = $db->sql_fetchrow($result);

	$user_check = $row['username'];
	$filename = $row['user_avatar'];
	$avatar_type = $row['user_avatar_type'];
	$db->sql_freeresult($result);

	$user_id = $row['user_id'];

		print "
		<br />The user checked is: " . $user_check . ", user id is: " . $user_id . "<br />
		The user_avatar is: " . $row['user_avatar'] . "
		";

	if (isset($filename[0]) && $filename[0] === 'g')
	{
		$avatar_group = true;
		$filename = substr($filename, 1);
	}

	switch ($avatar_type)
	{
		case AVATAR_UPLOAD:
			echo 'Avatar type = Upload<br />';
			echo 'row avatar width = ' . $row['user_avatar_width'] . '<br />';
			echo 'row avatar height = ' . $row['user_avatar_height'] . '<br />';
			$ext		= substr(strrchr($filename, '.'), 1);
			$stamp		= (int) substr(stristr($filename, '_'), 1);
			$filename	= (int) $filename;
			$prefix		= $config['avatar_salt'] . '_';
			$full_path	= $config['avatar_path'] . '/' . $prefix . $filename . '.' . $ext;
			$exists = file_exists($full_path);
			echo $full_path . (($exists) ? ' <font color="#33CC00"><b>exists</b></font>' : ' <font color="#ff0000"><b>does not exist</b></font>') . '<br /><br />';
			echo '<img src="http://' . $config['server_name'] . $config['script_path'] . '/download/file.php?avatar=' . $row['user_avatar'] . '">';
			echo '<br />Above image is produced directly by file.php<br />The image below is produced by function get_user_avatar<br />';
			$view_avatar = ($user->optionget('viewavatars')) ? get_user_avatar($row['user_avatar'], $row['user_avatar_type'], $row['user_avatar_width'], $row['user_avatar_height']) : 'Member has set profile to disable viewing of avatars';
			echo $view_avatar . '<br />';

			if (!$exists)
			{
				echo 'Avatar Salt is: ' . $config['avatar_salt'] . '<br />';
				echo 'user_avatar is: ' . $row['user_avatar'] . '<br />';
//				echo 'http://' . $config['server_name'] . $config['script_path'] . '/download/file.php?avatar=' . $user_avatar;
			}
		break;

		case AVATAR_REMOTE:
			echo 'Avatar type = Remote<br />';
//			$exists		= file_exists($full_path);
			echo 'Remote avatar full path = ' . $row['user_avatar'] . '<br />';
			print "
			<img src=" . $full_path . " title=" . $full_path . " alt = " . $full_path . ">
			";
		break;

		case AVATAR_GALLERY:
			echo 'Avatar type = Gallery<br />';
			$full_path	= $config['avatar_gallery_path'] . '/' . $filename;
			$exists = file_exists($full_path);
			echo $full_path . (($exists) ? ' <font color="#33CC00"><b>exists</b></font><br />' : ' <font color="#ff0000"><b>does not exist</b></font><br />');
		break;
	}

		$allow_avatar = (empty($config['allow_avatar'])) ? 'Avatars are NOT allowed' : 'Avatars ARE allowed';
		$allow_avatar_upload = (empty($config['allow_avatar_upload'])) ? 'Uploaded avatars are NOT allowed' : 'Uploaded avatars ARE allowed';
		$allow_avatar_remote = (empty($config['allow_avatar_remote'])) ? 'Remote avatars are NOT allowed' : 'Remote avatars ARE allowed';
		$allow_avatar_local = (empty($config['allow_avatar_local'])) ? 'Gallery avatars are NOT allowed' : 'Gallery avatars ARE allowed';
		$allow_avatar_remote_upload = (empty($config['allow_avatar_remote_upload'])) ? 'Remote uploaded avatars are NOT allowed' : 'Remote uploaded avatars ARE allowed';

		print "
		<p>$allow_avatar</p>
		<p>$allow_avatar_upload</p>
		<p>$allow_avatar_remote</p>
		<p>$allow_avatar_local</p>
		<p>$allow_avatar_remote_upload</p>
		";

	}

?>

<!-- you *can* remove this footer if you like -->
<div align="center">phpBB3 Avatar Checking script by <a href="http://www.phpbb.com/community/memberlist.php?mode=viewprofile&u=163542">Dicky</a><br />
Copyright &copy; 2010 Foote Enterprises</div>

</body>
</html>